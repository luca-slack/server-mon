#!/usr/bin/env python3

import argparse, logging, subprocess
from html import escape
from http.server import BaseHTTPRequestHandler, HTTPServer

# Defaults.
DEBUG = False
DEFAULT_LISTEN = "127.0.0.1"
DEFAULT_PORT = 5000

# User agent
USERAGENT = "Bot/server-mon"

# Log format.
LOG_FORMAT = "[%(asctime)-15s] %(message)s"

# Info to show.
COMMANDS = (
    # OS
    ["free", "-m", ],
    ["df", "-h", ],
    ["netstat", "-tan", ],

    # Docker
    ["docker", "ps", ],
    ["docker", "stats", "--no-stream", ],
    ["docker", "images", ],
    ["docker", "volume", "ls", ],
    ["docker", "version", ],

    # Websites
    ["curl", "-I", "--max-time", "2", "--user-agent", USERAGENT, "http://www.galardiosteopatia.it/", ],
)

# Style asset file.
ASSET_FILE = "style.css"

# HTML template.
TEMPLATE = (
    "<!DOCTYPE html>",
    "<html>",
        "<head>",
            "<title>Service Info</title>",
            "<link rel=\"stylesheet\" href=\"{}\">".format(ASSET_FILE),
        "</head>",
        "<body>",
            "<h1>Service Info</h1>",
            None,
        "</body>",
    "</html>",
)

# One for each `COMMANDS`, they go into `TEMPLATE` instead of `None`.
OUTPUT_FORMAT = "<div><h2>&gt; {cmdline}</h2><pre>{output}</pre></div>"

def exec_cmd(cmd):
    """Execute command and return its standard output."""
    logging.info("Executing command: %s", cmd)

    try:
        pipe = subprocess.run(cmd, stdout=subprocess.PIPE)
    except Exception as err:
        logging.exception(err)
        return err.__class__.__name__

    return pipe.stdout.decode()

class RequestHandler(BaseHTTPRequestHandler):
    ASSET_BODY = None

    def do_GET(self):
        """Handle GET request."""
        if self.path == "/" + ASSET_FILE:
            self.emit_asset()
        else:
            self.emit_response()

    def emit_asset(self):
        """Serve asset file."""
        self.send_response(200)
        self.send_header("Content-type", "text/css; charset=UTF-8")
        self.end_headers()

        if DEBUG or self.ASSET_BODY is None:
            logging.debug("Loading asset file")

            # Read asset file.
            with open(ASSET_FILE, "r") as asset:
                self.ASSET_BODY = asset.read()

        self.my_write(self.ASSET_BODY)

    def emit_response(self):
        """Render HTML template and write to stdout."""
        self.send_response(200)
        self.send_header("Content-type", "text/html; charset=utf-8")
        self.end_headers()

        for html in TEMPLATE:
            if html is not None:
                # String of HTML.
                self.my_write(html)
                continue

            # Execute command.
            for cmd in COMMANDS:
                self.my_write(OUTPUT_FORMAT.format(**{
                    "cmdline":  " ".join(cmd),
                    "output": escape(exec_cmd(cmd)),
                }))

    def my_write(self, byte_string):
        """Send output to socket."""
        self.wfile.write(byte_string.encode("utf-8"))

class ServiceInfo:
    def __init__(self, **kwargs):
        """Initialize."""
        self.address = kwargs.get("address") or DEFAULT_LISTEN
        self.port = kwargs.get("port") or DEFAULT_PORT
        logging.debug("Initialized server with %s", kwargs)

    def listen(self):
        """Start serving requests."""
        server_address = (self.address, self.port, )
        logging.info("Starting server...")

        httpd = HTTPServer(server_address, RequestHandler)
        logging.info("Listening to %s:%s", *server_address)

        httpd.serve_forever()

if __name__ == "__main__":
    # Prepare cli arguments parser.
    parser = argparse.ArgumentParser()
    parser.add_argument("--address", help="Listen to specified address", default=DEFAULT_LISTEN)
    parser.add_argument("--port", help="Listen to specified port", default=DEFAULT_PORT)
    parser.add_argument("--debug", help="Log debug info", action='store_true')

    # Parse arguments.
    args = parser.parse_args()

    if args.debug:
        DEBUG = True
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO

    logging.basicConfig(format=LOG_FORMAT, level=log_level)

    # Create.
    server = ServiceInfo(address=args.address, port=args.port)

    # Run!
    server.listen()
